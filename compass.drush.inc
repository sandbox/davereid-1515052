<?php

/**
 * Implements hook_drush_command().
 */
function compass_drush_command() {
  $items['compass-compile'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => 'Compile',
    'examples' => array(
      'drush compass-compile sitetheme' => 'Compile the SASS for the sitetheme theme.',
    ),
    'arguments' => array(
      'project' => array("The theme or module to compile. Omit this argument to compile the current site's default theme."),
    ),
    'allow-additional-options' => TRUE,
    'topics' => array('compass'),
    'options' => array(
      'time' => 'Display compilation times.',
      'quiet' => 'Quiet mode.',
      'trace' => 'Show a full stacktrace on error.',
      'force' => 'Allows some failing commands to succeed instead.',
      'dry-run' => 'Dry Run. Tells you waht it plans to do.',
    ),
    'callback' => 'drush_compass_compile',
  );

  return $items;
}

function drush_compass_compile($project = NULL) {
  if (!isset($project)) {
    $project = drush_theme_get_default();
  }

  $dir = drupal_get_path('theme', $project);
  if (empty($dir)) {
    $dir = drupal_get_path('module', $project);
    if (empty($dir)) {
      return drush_set_error(dt("Could not find project @project.", array('@project' => $project)));
    }
  }
  $dir = drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $dir;

  $command = 'compass compile ' . drush_escapeshellarg($dir);

  // The DRUSH_COMMAND_ARGS contain all args and options that appear after the command name.
  if ($args = drush_get_context('DRUSH_COMMAND_ARGS', array())) {
    for ($x = 0; $x < sizeof($args); $x++) {
      // escape all args except for command separators.
      if (!in_array($args[$x], array('&&', '||', ';'))) {
        $args[$x] = drush_escapeshellarg($args[$x]);
      }
    }
    //var_export($args);
    //$command .= ' ' . implode(' ', $args);
  }

  drush_log($command, 'notice');
  return drush_shell_proc_open($command);
}
